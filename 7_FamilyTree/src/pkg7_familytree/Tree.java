/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg7_familytree;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Benjamin
 */
public class Tree {
    private final Map<Integer, Personne> personnes;
    private final Map<Integer, Association> associations;
	
    public Tree() {
	this.personnes = new HashMap<Integer, Personne>();
	this.associations = new HashMap<Integer, Association>();
    }
    
    public void rajoutePersonne(Personne pers) {
	this.personnes.put(pers.id, pers);
    }
    
    public void rajouteAssociation(Association asso) {
	this.associations.put(asso.id, asso);
    }
}
