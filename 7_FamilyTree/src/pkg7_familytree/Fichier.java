/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg7_familytree;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Benjamin
 */
public class Fichier {
    public static void importData(Tree monArbre) throws FileNotFoundException {
	int done = 0;
	String type = "", entete = "", detail = "";
	
	int id = 0, idFamc = 0;
	String nom = "", prenom = "", naisslieu = "", deceslieu = "";
	String naissdate = "", decesdate = "";
	
	int idFam = 0;
	int husb = 0, wife = 0;
	String famidate = "", famiplace = "";
	
	Scanner s = new Scanner(new File("save.txt"));
	
	while (s.hasNext()) { //s.hasNext()
	    if (done == 0) {
		type = s.next();
	    }
	    
	    if (type.equals("INDI")) {
		done = 1;
		if (type.equals("INDI") && id == 0) {
		    id = s.nextInt();
		    System.out.println("ID " + id);
		}
		switch (entete) {
		    case "NAME" :
			prenom = s.next();
			nom = s.next();
			
			System.out.println("Prénom " + prenom);
			System.out.println("Nom " + nom);
			
			entete = s.next();
			break;
			
		    case "BIRT" :
			detail = s.next();
			if (detail.equals("DATE")) {
			    naissdate = s.next();
			    System.out.println("Date de naissance " + naissdate);
			}
			else if (detail.equals("PLAC")) {
			    naisslieu = s.next();
			    System.out.println("Lieu de naissance " + naisslieu);
			    
			    entete = s.next();
			}
			break;
			
		    case "DEAT" :
			detail = s.next();
			if (detail.equals("DATE")) {
			    decesdate = s.next();
			    System.out.println("Date de décès " + decesdate);
			}
			else if (detail.equals("PLAC")) {
			    deceslieu = s.next();
			    System.out.println("Lieu de décès " + deceslieu);
			    
			    entete = s.next();
			}
			break;
			
		    case "FAMC" :
			idFamc = s.nextInt();
			System.out.println("ID Famille " + idFam);
			done = 0;
			id = 0;
			
			Personne pers = new Personne(nom, prenom, naissdate, naisslieu, decesdate, deceslieu);
			monArbre.rajoutePersonne(pers);
			
			System.out.println("=-=-=-=-=-=-=-=-=-=");
			
			entete = s.next();
			break;
		
		    default : 
			entete = s.next();
			break;
		}
	    }
	    
	    else if (type.equals("FAMI")) {
		done = 1;
		if (type.equals("FAMI") && idFam == 0) {
		    idFam = s.nextInt();
		    System.out.println("ID Famille " + idFam);
		}
		switch (entete) {
		    case "HUSB" :
			husb = s.nextInt();
			System.out.println("Mari " + husb);
			
			entete = s.next();
			break;
			
		    case "WIFE"	:
			wife = s.nextInt();
			System.out.println("Epouse " + wife);
			
			entete = s.next();
			break;
			
		    case "DATE" :
			famidate = s.next();
			System.out.println("Date d'union " + famidate);
			
			entete = s.next();
			break;
			
		    case "PLAC" :
			famiplace = s.next();
			System.out.println("Lieu d'union " + famiplace);
			
			entete = s.next();
			done = 0;
			idFam = 0;
			
			Association asso = new Association(famidate, famiplace, husb, wife);
			monArbre.rajouteAssociation(asso);
			
			System.out.println("=-=-=-=-=-=-=-=-=-=");
			
			break;
			
		    default :
			entete = s.next();
			break;
		}
	    }
	    
	    else {
		done = 0;
	    }
	}
	
	s.close();
    }
}
