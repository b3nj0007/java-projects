/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg10_rectangles;

import java.util.Scanner;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import javafx.scene.shape.*;

/**
 *
 * @author Benjamin
 */
public class Main extends Application {
    
    @Override
    public void start(Stage primaryStage) {
	
	Scanner scan = new Scanner(System.in);
	
	System.out.println("Entrez les données pour le rectangle 1");
	System.out.print("[X] [Y] [W] [H] : ");
	
	Rectangle rect1 = new Rectangle(
	    scan.nextInt(),
	    scan.nextInt(),
	    scan.nextInt(),
	    scan.nextInt()
	);
	    rect1.setFill(Color.ORANGE);
	    rect1.setOpacity(0.50f);
	
	System.out.println("Entrez les données pour le rectangle 2");
	System.out.print("[X] [Y] [W] [H] : ");
	
	Rectangle rect2 = new Rectangle(
	    scan.nextInt(),
	    scan.nextInt(),
	    scan.nextInt(),
	    scan.nextInt()
	);
	    rect2.setFill(Color.VIOLET);
	    rect2.setOpacity(0.60f);
	    
	Rectangle rect3 = new Rectangle();
	    rect3 = inter(rect1, rect2);
	    rect3.setFill(Color.TOMATO);
	    rect3.setOpacity(0.75f);
	
	TextField text1 = new TextField();
	    text1.setText("COUCOU");
	    text1.setEditable(false);
	
	Pane root = new Pane();
	root.getChildren().addAll(rect1, rect2, rect3, text1);
	
	Scene scene = new Scene(root, 500, 500);
	
	primaryStage.setTitle("Intersection rectangles");
	primaryStage.setScene(scene);
	primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
	launch(args);
    }
    
    double max(double val1, double val2) {
	if (val1 >= val2) return val1;
	else return val2;
    }
    
    double min(double val1, double val2) {
	if (val1 <= val2) return val1;
	else return val2;
    }
    
    public Rectangle inter(Rectangle rect1, Rectangle rect2) {
	Rectangle retour = new Rectangle(   
	    max(rect1.getX(), rect2.getX()),
	    max(rect1.getY(), rect2.getY()),
	    min(rect1.getX() + rect1.getWidth(), rect2.getX() + rect2.getWidth()) - max(rect1.getX(), rect2.getX()),
	    min(rect1.getY() + rect1.getHeight(), rect2.getY() + rect2.getHeight()) - max(rect1.getY(), rect2.getY())
	);
	
	return retour;
    }
}
