/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

/**
 *
 * @author Benjamin
 */
public class Calculator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
	
	int result = 0;
	
	switch (args[1]) {
	    case "+" : case "add" : {
		result = (Integer.parseInt(args[0]) + Integer.parseInt(args[2]));
		break;
	    }
	    
	    case "-" : case "sub" : {
		result = (Integer.parseInt(args[0]) - Integer.parseInt(args[2]));
		break;
	    }
	    
	    case "/" : case "div" : {
		result = (Integer.parseInt(args[0]) / Integer.parseInt(args[2]));
		break;
	    }
	    
	    case "." : case "*" : case "mul" : {
		result = (Integer.parseInt(args[0]) * Integer.parseInt(args[2]));
		break;
	    }
	    
	    default : {
		System.out.println("Wrong sign !");
	    }
	}
	
	for (int i = 0; i <= 2; i++) {
	    System.out.print(args[i] + " ");
	}
	System.out.println("= " + result);
    }
    
}
