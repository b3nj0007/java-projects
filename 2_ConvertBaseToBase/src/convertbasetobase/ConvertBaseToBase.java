/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package convertbasetobase;

import java.util.Scanner;

/**
 *
 * @author Benjamin
 */
public class ConvertBaseToBase {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
	Scanner scan1 = new Scanner(System.in);
	
	System.out.println("Salut !");
	System.out.print("Entre [NOMBRE] [BASE] [BASE]: ");
	
	String num = scan1.next();
	String base1 = scan1.next();
	String base2 = scan1.next();
	scan1.close();
	
	int result = 0;
	
	switch (base1) {
	    case "bin" : {
		result = Integer.parseInt(num, 2);
		break;
	    }
	    
	    case "dec" : {
		result = Integer.parseInt(num, 10);
		break;
	    }
	    
	    case "hex" : {
		result = Integer.parseInt(num, 16);
		break;
	    }
	    
	    default : {
		System.out.println("BASE1 incorrecte !");
		break;
	    }
	}
	
	System.out.print(num + " en base " + base1 + " donne ");
	
	switch (base2) {
	    case "bin" : {
		System.out.print(Integer.toBinaryString(result));
		break;
	    }
	    
	    case "dec" : {
		System.out.print(result);
		break;
	    }
	    
	    case "hex" : {
		System.out.print(Integer.valueOf(String.valueOf(result), 16));
		break;
	    }
	    
	    default : {
		System.out.println("BASE2 incorrecte !");
		break;
	    }
	}
	
	System.out.println(" en base " + base2);
    }
    
}
