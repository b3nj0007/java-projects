/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg5_chronometre;

/**
 *
 * @author Benjamin
 */
public class Main {
    
    public static void delay(int _wait) {
	try {
	    Thread.sleep(_wait);
	}
	
	catch(InterruptedException ex) {
	    Thread.currentThread().interrupt();
	}
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
	
	Stopwatch chrono1 = new Stopwatch();
	chrono1.start();
	
	for (int i = 0; i < 50; i++) {
	    delay(100);
	    System.out.println(chrono1.getElapsedTime());
	    if (i == 34) chrono1.stop();
	}
	
	System.out.println("GET start :: " + chrono1.getStartTime());
	System.out.println("GET end   :: " + chrono1.getEndTime());
	System.out.println("DELTA :: " + chrono1.getElapsedTime());
	System.out.println();
	
	System.out.println("Generating random int array...");
	int[] tbl = new int[100000];
	
	for (int i = 0; i < tbl.length; i++) {
	    tbl[i] =(int) (Math.random() * 1000);
	}
	
	//for (int i = 0; i < tbl.length; i++) {
	//    System.out.println(tbl[i]);
	//}
	
	Stopwatch chrono2 = new Stopwatch();
	System.out.println("Begin sort");
	chrono2.start();
	
	for (int i = 0; i < tbl.length; i++) {
	    int min = i;
	    
	    for (int j = i + 1; j < tbl.length; j++) {
		if (tbl[j] < tbl[min]) {
		    min = j;
		}
	    }
	    
	    if (min != i) {
		int swap = tbl[i];
		tbl[i] = tbl[min];
		tbl[min] = swap;
	    }
	}
	
	chrono2.stop();
	System.out.println("Sort complete!");
	
	//for (int i = 0; i < tbl.length; i++) {
	//    System.out.println(tbl[i]);
	//}
	
	System.out.println("Array was sorted after " + chrono2.getElapsedTime() + " ms.");
    }
}
