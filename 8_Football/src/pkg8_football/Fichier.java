package pkg8_football;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Fichier {

	public void lit(String nomFichier, Championnat champ) throws FileNotFoundException {
		int numero = 0, groupe = 0;
		String nom = null, ville = null;
	
		Scanner s = new Scanner(new File(nomFichier));
		while (s.hasNext()) {
			String entete = s.next();
			if (entete.equals("EQUIPE")) {
				numero = s.nextInt();
				nom = s.next();
			} else if (entete.equals("VILLE")) {
				ville = s.next();
			} else if (entete.equals("GROUPE")) {
				groupe = s.nextInt();
				Equipe eq = new Equipe(nom, ville, numero);
				champ.rajoute(eq, groupe);
			}
		}
		s.close();
	}

	public void ecrit(String nomFichier, Championnat champ) {
		
	}
}
