package pkg8_football;

import java.util.LinkedList;
import java.util.List;


public class Groupe {

	private int numero;
	private final List<Equipe> equipes;

	public Groupe(int groupe) {
		this.numero = groupe;
		this.equipes = new LinkedList<Equipe>();
	}

	public int getNumero() {
		return this.numero;
	}

	public void rajoute(Equipe eq) {
		this.equipes.add(eq);
	}

	public void afficheEquipes() {
		for (Equipe e : this.equipes) {
			e.affiche();
		}
	}

}
