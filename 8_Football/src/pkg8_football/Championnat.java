package pkg8_football;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Championnat {

    private final Map<String, Equipe> equipes;
    private final Map<Integer, Groupe> groupes;

    public Championnat() {
	this.equipes = new HashMap<String,Equipe>();
	this.groupes = new HashMap<Integer,Groupe>();
    }

    public void rajoute(Equipe eq, int groupe) {
	this.equipes.put(eq.getNom(), eq);
	Groupe g = this.groupes.get(groupe);
	if (g == null) {
		g = new Groupe(groupe);
		this.groupes.put(groupe,  g);
	}
	eq.rejoint(g);
	g.rajoute(eq);
    }

    public void afficheGroupes() {
	for (Groupe g : this.groupes.values()) {
		g.afficheEquipes();
	}
    }

    public static void main(String[] args) throws FileNotFoundException {
	System.out.println("Chamiponnat commence!");
	Championnat c = new Championnat();
	Fichier f = new Fichier();
	f.lit("championnat.txt", c);
	Utilisateur u = new Utilisateur();
	u.traite(c);
	f.ecrit("championnat.txt", c);
    }
}
