package pkg8_football;

import java.util.Scanner;

public class Utilisateur {

	public void traite(Championnat champ) {
		Scanner s = new Scanner(System.in);
		System.out.println("Votre commande:");
		while (s.hasNext()) {
			String cmd = s.next();
			if (cmd.equals("aff")) {
				champ.afficheGroupes();
			} else if (cmd.equals("fin")) {
				return;
			} else {
				System.out.println("Commandes:");
				System.out.println("aff: affiche");
				System.out.println("fin: termine");
			}
			System.out.println("Votre commande:");
		}
	}

}
