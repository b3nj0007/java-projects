/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg6_matrix;

/**
 *
 * @author Benjamin
 */
public class Matrix {
    private final int rows = 1;
    private final int columns = 1;
    private final double[][] value = {};
    
    public Matrix(int i, int j) {
	for (i = 0; i < value.length; i++) {
	    for (j = 0; j < value.length; j++) {
		value[i][j] = 0;
	    }
	}
    }
    
    public Matrix(double[][] data) {
	for (int i = 0; i < data.length; i++) {
	    for (int j = 0; j < data.length; j++) {
		value[i][j] = data[i][j];
	    }
	}
    }

    public int getRows() {
	return this.rows;
    }

    public int getColumns() {
    	return this.columns;
    }

    public Matrix multiply(Matrix b) {
	if (this.columns != b.columns) {
	    return b;
	}
	else {
	    Matrix result = new Matrix(this.getRows(), b.getColumns());

	    for (int i = 0; i < b.columns; i++) {
		for (int j = 0; j < b.rows; j++) {
		    result.value[i][j] += this.value[i][j] * b.value[i][j];
		}
	    }

	    return result;
	}
    }

    public Matrix multiply(double d) {
	Matrix result = new Matrix(this.rows, this.columns);
	
    	for (int i = 0; i < this.getRows(); i++) {
	    for (int j = 0; j < this.getColumns(); j++) {
		result.value[i][j] = this.value[i][j] * d;
	    }
	}
	
	return result;
    }

    public Matrix add(Matrix c) {
	Matrix sum = new Matrix(this.rows, this.columns);
	for (int i = 0; i < this.rows; i++) {
	    for (int j = 0; j < this.columns; j++) {
		sum.value[i][j] = this.value[i][j] + c.value[i][j];
	    }
	}
	return sum;
    }

    public Matrix subtract(Matrix d) {
	Matrix sub = new Matrix(this.rows, this.columns);
	for (int i = 0; i < this.rows; i++) {
	    for (int j = 0; j < this.columns; j++) {
		sub.value[i][j] = this.value[i][j] - d.value[i][j];
	    }
	}
	return sub;
    }

    public Matrix transpose() {
	Matrix transp = new Matrix(this.rows, this.columns);
	
	for (int i = 0; i < value.length; i++) {
	    for (int j = 0; j < value.length; j++) {
		double[] value1 = value[j];
	    }
	}
	
	return transp;
    }

    public double determinant() {
	if (this.rows != this.columns)
	    return 0;
	if (this.rows == 1)
	    return this.value[0][0];
	if (this.rows == 2)
	    return this.value[0][0] * this.value[1][1] - this.value[0][1] * this.value[1][0];

	double sum = 0.0;
	for (int i = 0; i < this.rows; i++) {
		sum += signal(i) * this.value[0][i] * subMatrix(0, i).determinant();
	}
	return sum;
    }

    public Matrix subMatrix(int excludeRow, int excludeColumn) {
	Matrix mat = new Matrix(this.rows - 1, this.columns - 1);
	int r = -1;
	for (int i = 0; i < this.rows; i++) {
	    if (i != excludeRow) {
		r++;
		int c = -1;
		for (int j = 0; j < this.columns; j++) {
		    if (j != excludeColumn) {
			c++;
			mat.value[r][c] = this.value[i][j];
		    }
		}
	    }
	}
	return mat;
    }

    private double signal(int i) {
	return ((i&1)==0) ? 1. : -1.;
    }

    public Matrix cofactor() {
	if (this.rows != this.columns)
	    return null;

	Matrix mat = new Matrix(this.rows, this.columns);
	for (int i = 0; i < this.rows; i++) {
	    for (int j = 0; j < this.columns; j++) {
		Matrix m1 = subMatrix(i, j);
		double det = m1.determinant();
		mat.value[i][j] = signal(i) * signal(j) * det;
	    }
	}
	return mat;
    }

    public Matrix inverse() {
	return cofactor().transpose().multiply(1. / determinant());
    }

    public String convertToString() {
	String result = null;
	
	return result;
    }
}
